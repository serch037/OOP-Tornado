#! /usr/bin/env python
# -*- coding: utf-8 -*-
import pandas
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d as Axes3D
# vim:fenc=utf-8
#
# Copyright © 2016 sergio <sergio@evoMacSergio>
#
# Distributed under terms of the MIT license.

"""
This utility script plots values from the log
"""

file = open('log.txt', 'rb')
fields = ['Sensor unique ID', '[X, Y, Z]']
pFile = pandas.read_csv('log.txt', skiprows=0,  sep="|", usecols= [0,3], index_col= 0)
print(pFile.size)
print(pFile.iloc[6])
superList = {}
for index, row in pFile.iterrows():
    #print(index)
    if  index not in superList:
        superList[index] = []
        superList[index].append(row[0].split(','))
    else:
        superList[index].append(row[0].split(','))
    #print(pFile.loc[index])
    
fig = plt.figure()
ax = fig.add_subplot(111,projection='3d')

for key in superList:
    print(key)
    xList = []
    yList = []
    zList = []
    for value in superList[key]:
        print(value)
        xList.append(float(value[0]))
        yList.append(float(value[1]))
        zList.append(float(value[2]))
    ax.scatter(xList,yList, zList)

#positionList = pFile['Position'].values.tolist()
    print
#  xList = []
#  yList = []
#  zList = []
#  for x in positionList:
#      xList.append(float(x.split(',')[0]))
#      yList.append(float(x.split(',')[1]))
#      zList.append(float(x.split(',')[2]))
#  print(xList)
#  print(yList)
#  print(zList)
#  fig = plt.figure()
#  ax = fig.add_subplot(111,projection='3d')
#  ax.plot(xList,yList, zList)
#  xList[1] = 10
#  ax.plot(xList,yList, zList)
plt.show()

