package com.itesm;
import com.itesm.sensors.*;
import java.util.ArrayList; 
import java.nio.file.*;
import java.nio.channels.FileChannel;
import java.io.IOException;
import java.nio.ByteBuffer;

public class Simulation
{
    public Simulation(){};
    public void run()
    {
        Path path =  Paths.get("log.txt");
        try
        {
        FileChannel fileChannel = FileChannel.open(path, StandardOpenOption.APPEND,StandardOpenOption.CREATE);
        fileChannel.truncate(0);
        String input = "A brief example 2\n";
        // String header = "Sensor unique ID\t| Sensor name\t\t| Measure (proper unit)\t |[X, Y, Z]\t\t\t\t| Battery left\n";
        String header = "ID|Sensor name|Measure|Position|Battery left\n";
        byte [] inputBytes = header.getBytes();
        ByteBuffer buffer = ByteBuffer.wrap(inputBytes);
        fileChannel.write(buffer);
        fileChannel.close();
        
        
        AngularSensor tmp = new AngularSensor("1", FileChannel.open(path, StandardOpenOption.APPEND,StandardOpenOption.CREATE));
        // AngularSensor tmp1 = new AngularSensor("2", FileChannel.open(path, StandardOpenOption.APPEND,StandardOpenOption.CREATE));
        //VerticalSpeedSensor tmp2 = new VerticalSpeedSensor("3", FileChannel.open(path, StandardOpenOption.APPEND,StandardOpenOption.CREATE));
        //VerticalAccelerationSensor tmp3 = new VerticalAccelerationSensor("4", FileChannel.open(path, StandardOpenOption.APPEND,StandardOpenOption.CREATE));
        ArrayList<Sensor > sensors = new ArrayList<>();
        sensors.add(tmp);
        // sensors.add(tmp1);
        // sensors.add(tmp2);
        // sensors.add(tmp3);
        
            try
            {
                for (Sensor e : sensors)
                {
                    e.myThread.join();
                    System.out.println(e.id);
                }
                
            }
            catch(InterruptedException e)
            {
                System.out.println("Interrupted");
                Thread.currentThread().interrupt();
            }
        }
        catch (IOException e)
        {
            System.out.println("File not opened");
        }
        System.out.println("Goodbye");
	}

}
