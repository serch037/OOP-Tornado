/*
 * VerticalSpeedSensor.java
 * Copyright (C) 2016 sergio <sergio@evoMacSergio>
 *
 * Distributed under terms of the MIT license.
 */
package com.itesm.sensors;
import com.itesm.Vector;
import java.nio.channels.FileChannel;
//import java.io.IOException;
//import java.nio.ByteBuffer;
//import com.itesm.Tornado;
import java.util.ArrayList;
import java.util.Random;

public class VerticalSpeedSensor extends Sensor
{
	public VerticalSpeedSensor(String anId, FileChannel _channel) {
		super(anId, _channel, "TS-1001-VS");
        Random rand = new Random();
        myThread = new Thread(this, name);
        discharge = rand.nextInt(2-1)+1;
        initializeSensors();
        myThread.start();
	}

    @Override 
    protected void calculate(double time)
    {
        measure = (float)getAvgLinSpeed(time);
    }

    @Override
    protected void initializeSensors()
    {
        
    }

    private double getAvgLinSpeed(double time)
    {
        return (position.getZ() - lastPos.getZ()) / (time - lastTime);
    }

    private double getInstLinSpeed()
    {
        //z differentiation
        return 0 ;
    }

    
}

