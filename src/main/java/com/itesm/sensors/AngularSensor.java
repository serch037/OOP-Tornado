
/*
 * AngularSensor.java
 * Copyrightk(C) 2016 sergio <sergio@evoMacSergio>
 *
 * Distributed under terms of the MIT license.
 */
package com.itesm.sensors;
import com.itesm.Vector;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Random;
public class AngularSensor extends  Sensor
{

   
    public AngularSensor(String anId, FileChannel _channel )
    { 
        super(anId, _channel, "TS-1001-AM");
        Random rand = new Random();
        discharge = rand.nextInt(4)+1;
        myThread = new Thread(this, name);
        initializeSensors();
        myThread.start();

    }




    @Override
    protected void calculate(double time)
    {
        System.out.println(getAvgLinSpeed(time) +" " + getInstLinSpeed(time));
        measure =(float)(position.getRPos()*mass*getAvgLinSpeed(time));
        //System.out.println(time);
        //measure =(float)((getInstLinSpeed(time)-getAvgLinSpeed(time))/(getInstLinSpeed(time)));
    }

   




    private double getInstLinSpeed(double time)
    {
        double r = position.getRPos();
        double theta = position.getTheta();
        //Derivative of  k*(log(t+1) + t)*r*theta with respect to t
        double linSpeed = r*theta*(100/(Math.pow(tornado.getHeight(), 2)) + tornado.getRadious()/(time+1));
        return linSpeed;
    }

    private double getAvgLinSpeed(double time)
    {
        double thetaThis = position.getTheta();
        double thetaLast = lastPos.getTheta(); 
        double linSpeed = (thetaThis-thetaLast)/(time-lastTime);
        //Does average r equals the arithmetic average of r?
        return linSpeed*(Math.abs(position.getRPos())+Math.abs(lastPos.getRPos()))/2;
        //return linSpeed;
    }

    @Override
    protected void initializeSensors()
    {
        components = new ArrayList<Component>();
        for (int i = 0; i < 10 ; i++ ) 
        {
            components.add(new Component("Distance"));
            components.add(new Component("Angular"));
            components.add(new Component("Mass"));
        }
        
        for (int i = 0; i < 10; i++) 
        {
            components.add(new Component("Distance"));
        }
    }

}

