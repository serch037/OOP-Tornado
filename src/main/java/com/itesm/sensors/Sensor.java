/*
 * Sensor.java
 * Copyright (C) 2016 sergio <sergio@evoMacSergio>
 *
 * Distributed under terms of the MIT license.
 * TODO: Change to Abstract Interface
 */
package com.itesm.sensors;
import com.itesm.Tornado;
import com.itesm.Vector;
import java.nio.channels.FileChannel;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
public abstract class  Sensor implements  Runnable
{
    public String id;
    String name;
    float measure;
    Vector position;
    float battery;
    public Thread myThread;
    protected StringBuilder record;
    protected long startTime;
    protected float updateInterval;
    protected FileChannel fileChannel;
    protected static final Tornado tornado;
    protected ArrayList<Component> components;
    protected int discharge;
    protected int mass;
    protected Vector lastPos;
    protected double lastTime;
//    public void live();
    
    static{
        tornado = new Tornado(50,20,10);
    }

    public Sensor(String an_Id, FileChannel _channel, String aName)
    {
        mass = 250;
        id = an_Id;
        name = "TS-1001-AM";
        fileChannel = _channel;
        measure = 0;
        position = new Vector(0,0,0);
        lastPos = position;
        battery = 100;
        //record = new StringBuilder();
        startTime = System.nanoTime();
        updateInterval = 5;
        fileChannel = _channel;

        
    }

    // public abstract void run();
    protected abstract void calculate(double time);
    //protected abstract void getNewPosition(long time);
    protected abstract void initializeSensors();

    /*
       Sensor unique ID
       Sensor name
       Measure (proper unit)
       [X, Y, Z]
       Battery left
       */

    public String createPrintableReport()
    {
        StringBuilder tmp = new StringBuilder();
        tmp.append(id)
            .append("\t")
            .append(name)
            .append("\t")
            .append(measure)
            .append("\t")
            .append(position.toString())
            .append("\t")
            .append(battery);
            //record.append(tmp)
        return tmp.toString();
    }

//    @Override
/*
    public String reportWrite()
    {
        StringBuilder tmp = new StringBuilder();
        tmp.append(id)
            .append("\t\t\t\t\t| ")
            .append(name)
            .append("\t\t| ")
            .append(measure)
            .append("\t\t\t\t\t| ")
            .append(position.toString())
            .append("\t\t\t| ")
            .append(battery)
            //record.append(tmp)
            .append("\n");
        return tmp.toString();
    }
    */

 public String createLoggableReport()
    {
        StringBuilder tmp = new StringBuilder();
        tmp.append(id)
            .append("|")
            .append(name)
            .append("|")
            .append(measure)
            .append("|")
            .append(position.toString())
            .append("|")
            .append(battery)
            .append("\n");
        return tmp.toString();
    }


    public void logReport()
    {
        String report = createLoggableReport();
        try
        {
            byte[] inputBytes = report.getBytes();
            ByteBuffer buffer = ByteBuffer.wrap(inputBytes);
            fileChannel.write(buffer);
        }
        catch(IOException e)
        {
            System.out.println("Error wrtiting report");
        }
    }

        public long getDeltaTime()
    {
        return System.nanoTime() - startTime;
    }

    public double getSeconds(long time)
    {
        return  time/1000000000.0;
    }

    @Override
    public void run()
    {
        System.out.println("Starting");
        try 
        {
            while(battery >0)
            {
                double time = getSeconds(getDeltaTime());
                getNewPosition(time);
                calculate(time);
                String tmp = createPrintableReport();
                //System.out.println(tmp);
                logReport();
                battery -= discharge*updateInterval;
                lastPos = position;
                lastTime = time;
                Thread.sleep((int) (updateInterval*1000));
            }
        }
        catch (InterruptedException e)
        {
            System.out.print("Stopped");
            Thread.currentThread().interrupt();
        }
    }


    protected void getNewPosition(double time)
    {
        position = tornado.moveSensor(position, time);
    }
}

