/*
 * Tornado.java
 * Copyright (C) 2016 sergio <sergio@evoMacSergio>
 *
 * Distributed under terms of the MIT license.
 */

package com.itesm;
import com.itesm.Vector;
import java.util.Random;
public  class Tornado
{
    private final int height, radious, intensity;
    private Random random;

    public Tornado(int height, int radious, int intensity){
        this.height = height;
        this.radious = radious;
        this.intensity = intensity;
        random = new Random();
    }


    public Vector moveSensor(Vector aVector, double time)
    {
        //double seconds =  (double)time / 1000000000.0;
        double seconds = time;
/*         float x = aVector.getX()+(float)seconds; */
        // float y = aVector.getY()+(float)seconds;
        /* float z = aVector.getZ()+(float)seconds; */
        float radian = (float)Math.PI/2*((float)seconds)+ aVector.orig_radian;

        float z = (float)seconds/height*100;
        float newRadi = radious*(float)(Math.log(seconds+1) + z/height);
        float x = newRadi*(float)Math.cos(radian);
        float y = newRadi*(float)Math.sin(radian);
        return new Vector(x, y, z); 
    }

    public int getRadious()
    {
        return this.radious;
    }

    public int getHeight()
    {
        return this.height;
    }

}

