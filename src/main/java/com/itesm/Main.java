/*
 * Main.java
 * Copyright (C) 2016 sergio <sergio@evoMacSergio>
 *
 * Distributed under terms of the MIT license.
 */
package com.itesm;

//TODO: ArrayList of interface
public class Main
{
	public static void main(String[] args) {
        Simulation sim = new Simulation();
        sim.run();
    }

}

