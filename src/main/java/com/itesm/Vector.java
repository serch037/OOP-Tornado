/*
 * Vector.java
 * Copyright (C) 2016 sergio <sergio@evoMacSergio>
 *
 * Distributed under terms of the MIT license.
 */

package com.itesm;
import java.util.Random;
public class Vector
{
    private float x;
    private float y;
    private float z;
    private float radian;
    public final float orig_radian;

	public Vector() 
    {
        x = 0;
        y = 0;
        z = 0;
        orig_radian = 0;
        radian = orig_radian;
	}
    
    public Vector(float someX, float someY, float someZ)
    {
        Random rand = new Random();
        x = someX;
        y = someY;
        z = someZ;
        orig_radian = rand.nextFloat()*2*(float)Math.PI;
        radian = orig_radian;
	}

    public Vector(float someX, float someY)
    {
        x = someX;
        y = someY;
        z = 0;
        orig_radian = 0;
        radian = orig_radian; 
	}

    public Vector add(float val)
    {
        return new Vector(x+val, y+val, z+val);
    }

    public float getX()
    {
        return this.x;
    }

    public float getY()
    {
        return this.y;
    }

    public float getZ()
    {
        return this.z;
    }

    public float getRadian()
    {
        return this.radian;
    }

    public  double getRPos()
    {
        return Math.sqrt(Math.pow(this.x, 2)+ Math.pow(this.y,2));
    }

    public  double getTheta()
    {
        return Math.atan2(this.y, this.x);
    }

    @Override
    public String toString()
    {
        StringBuilder tmp = new StringBuilder();
        tmp.append(x)
            .append(", ")
            .append(y)
            .append(", ")
            .append(z);
        return tmp.toString();
    }
}

