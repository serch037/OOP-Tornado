(TeX-add-style-hook
 "report"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "12pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("babel" "english") ("inputenc" "utf8x") ("todonotes" "colorinlistoftodos") ("biblatex" "sytle=ieee") ("caption" "font=scriptsize") ("subfig" "caption=false" "font=footnotesize") ("xcolor" "table" "xcdraw")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art12"
    "babel"
    "inputenc"
    "amsmath"
    "amssymb"
    "amsthm"
    "amsfonts"
    "graphicx"
    "todonotes"
    "siunitx"
    "fontspec"
    "microtype"
    "lmodern"
    "biblatex"
    "unicode-math"
    "parskip"
    "caption"
    "subfig"
    "fixltx2e"
    "booktabs"
    "xcolor"
    "float"
    "URL")
   (TeX-add-symbols
    "HRule")
   (LaTeX-add-bibliographies
    "PBL1"))
 :latex)

